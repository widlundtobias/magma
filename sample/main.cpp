#include <ast.hpp>
#include <interpreter.hpp>
#include <tokenizer.hpp>
#include <parser.hpp>
#include <iostream>

#include <chrono>

int32_t main()
{
    auto time1 = std::chrono::high_resolution_clock::now();
    std::string codeString =
R"foo(
{
    var int gloka = 3 + 3;

    var int gloka3;
    gloka3 = 8;

    var int gloka5 = 10;

    var int result = gloka + (gloka3 * gloka5);

    result;
}
)foo";
    auto time2 = std::chrono::high_resolution_clock::now();

    std::cout << "input:\n" << codeString <<"\n";
    TokenData tokenizedCode = tokenize(codeString);
    std::cout << "tokens:\n";
    for(const auto& t : tokenizedCode.tokens)
        std::cout << toString(t) << " ";
    std::cout << "\n\n";
    auto time3 = std::chrono::high_resolution_clock::now();

    AST blockAST = parseBlock(tokenizedCode);
    std::cout << "ast:\n";
    for(const auto& t : blockAST.nodes)
        std::cout << toString(t) << " ";
    std::cout << "\n\n";
    auto time4 = std::chrono::high_resolution_clock::now();

    std::optional<LeafFrame> result = interpret(blockAST);
    auto time5 = std::chrono::high_resolution_clock::now();

    std::cout << "result was " << (result ? std::to_string(result->intVal) : std::string("none")) << "\n";
    std::cout << "times:\n";
    std::cout << "allocate: " << std::chrono::duration_cast<std::chrono::milliseconds>(time2 - time1).count()<< "ms\n";
    std::cout << "tokenize: " << std::chrono::duration_cast<std::chrono::milliseconds>(time3 - time2).count()<< "ms\n";
    std::cout << "parse: " << std::chrono::duration_cast<std::chrono::milliseconds>(time4 - time3).count()<< "ms\n";
    std::cout << "interpret: " << std::chrono::duration_cast<std::chrono::milliseconds>(time5 - time4).count()<< "ms\n";
    std::cout << "total " << std::chrono::duration_cast<std::chrono::milliseconds>(time5 - time1).count()<< "ms\n";

    std::cout << sizeof(ASTNode) << "\n";
}
