#pragma once
#include <vector>
#include <string_view>
#include <token.hpp>

TokenData tokenize(std::string_view input); //returns a list of all tokens
