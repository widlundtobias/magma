#include "token.hpp"

Keyword keywordFromString(std::string_view text)
{
    if(text == "var")
        return Keyword::Var;
    else if(text == "if")
        return Keyword::If;
    else if(text == "else")
        return Keyword::Else;
    else if(text == "for")
        return Keyword::For;
    else if(text == "return")
        return Keyword::Return;
    else if(text == "cast")
        return Keyword::Cast;

    return Keyword::NoKeyword;
}

bool operator==(const Identifier& a, const Identifier& b)
{
    return a.name == b.name;
}

bool operator==(const Literal& a, const Literal& b)
{
    return a.value == b.value;
}

bool operator==(const LineComment& a, const LineComment& b)
{
    return a.content == b.content;
}

bool operator==(const BlockComment& a, const BlockComment& b)
{
    return a.content == b.content;
}

std::string toString(Keyword keyword)
{
    if(keyword == Keyword::Var)
        return "var";
    else if(keyword == Keyword::If)
        return "if";
    else if(keyword == Keyword::Else)
        return "else";
    else if(keyword == Keyword::For)
        return "for";
    else if(keyword == Keyword::Return)
        return "return";
    else if(keyword == Keyword::Cast)
        return "cast";
    else {
        //assert
    }
}

std::string toString(Separator separator)
{
    if(separator == Separator::CurlyBStart)
        return "{";
    else if(separator == Separator::CurlyBEnd)
        return "}";
    else if(separator == Separator::RoundBStart)
        return "(";
    else if(separator == Separator::RoundBEnd)
        return ")";
    else if(separator == Separator::SquareBStart)
        return "[";
    else if(separator == Separator::SquareBEnd)
        return "]";
    else if(separator == Separator::Semicolon)
        return ";";
    else if(separator == Separator::Comma)
        return ",";
    else {
        //assert
    }
}

std::string toString(Operator oper)
{
    if(oper == Operator::Add)
        return "+";
    else if(oper == Operator::Subtract)
        return "-";
    else if(oper == Operator::Multiply)
        return "*";
    else if(oper == Operator::Divide)
        return "/";
    else if(oper == Operator::Assign)
        return "=";
    else {
        //assert
    }
}

std::string toString(const Token& token)
{
    if(std::holds_alternative<Identifier>(token))
        return "IDENT[" + std::string(std::get<Identifier>(token).name) + "]";
    else if(std::holds_alternative<Keyword>(token))
        return "KEYWD[" + toString(std::get<Keyword>(token)) + "]";
    else if(std::holds_alternative<Separator>(token))
        return "SEPAR[" + toString(std::get<Separator>(token)) + "]";
    else if(std::holds_alternative<Operator>(token))
        return "OPERA[" + toString(std::get<Operator>(token)) + "]";
    else if(std::holds_alternative<Literal>(token))
        return "LITER[" + std::string(std::get<Literal>(token).value) + "]";
    else if(std::holds_alternative<LineComment>(token))
        return "LINEC";
    else if(std::holds_alternative<BlockComment>(token))
        return "BLOCC";
    else {//assert
    }
}
