#include "tokenizer.hpp"
#include <algorithm>

bool isDigitChar(char c)
{
    return c >= 48 && c <= 57; //0-9
}

bool isNumericalChar(char c)
{
    return isDigitChar(c) ||
        c == '.' ||
        c == 'f'
    ;
}

bool isOperatorChar(char c)
{
    return c == '+' ||
        c == '-' ||
        c == '*' ||
        c == '/' ||
        c == '='
    ;
}

bool isSeparatorChar(char c)
{
    switch(c)
    {
        case '(':
        case ')':
        case '{':
        case '}':
        case '[':
        case ']':
        case ';':
        case ',':
            return true;
    }
    return false;
}

bool isIdentifierChar(char c)
{
    return
        (c >= 65 && c <= 90) || //A-Z
        (c >= 97 && c <= 122)||  //a-z
        c == 95 //_
    ;
};

TokenData tokenize(std::string_view input)
{
    TokenData result;
    result.tokens.reserve(input.size());
    result.debugInfo.reserve(input.size());

    size_t current = 0;

    size_t size = input.size();
    for(size_t current = 0; current < size; ++current)
    {
        char currentChar = input[current];

        if(currentChar == ' ')
            continue;

        if(isDigitChar(currentChar))
        {//start parsing numerical literal
            size_t literalEnd = current;
            char literalCurrent = currentChar;

            while(isNumericalChar(literalCurrent))
            {
                ++literalEnd;
                literalCurrent = input[literalEnd];
            }

            std::string_view value = input.substr(current, literalEnd - current);

            result.tokens.emplace_back(Literal{value});
            result.debugInfo.emplace_back(0, 0);

            current = literalEnd - 1; //set it to one before end since the forloop will do +1 anyway
        }
        else if(isOperatorChar(currentChar))
        {//start parsing an operator
            char operatorChar = currentChar;

            Operator oper;

            if(operatorChar == '+')
                oper = Operator::Add;
            else if(operatorChar == '-')
                oper = Operator::Subtract;
            else if(operatorChar == '*')
                oper = Operator::Multiply;
            else if(operatorChar == '/')
                oper = Operator::Divide;
            else if(operatorChar == '=')
                oper = Operator::Assign;

            result.tokens.emplace_back(oper);
            result.debugInfo.emplace_back(0, 0);
        }
        else if(isSeparatorChar(currentChar))
        {//start parsing an operator
            char separatorChar = currentChar;

            Separator separator;

            if(separatorChar == '(')
                separator = Separator::RoundBStart;
            else if(separatorChar == ')')
                separator = Separator::RoundBEnd;
            else if(separatorChar == '{')
                separator = Separator::CurlyBStart;
            else if(separatorChar == '}')
                separator = Separator::CurlyBEnd;
            else if(separatorChar == '[')
                separator = Separator::SquareBStart;
            else if(separatorChar == ']')
                separator = Separator::SquareBEnd;
            else if(separatorChar == ';')
                separator = Separator::Semicolon;
            else if(separatorChar == ',')
                separator = Separator::Comma;

            result.tokens.emplace_back(separator);
            result.debugInfo.emplace_back(0, 0);
        }
        else if(isIdentifierChar(currentChar))
        {
            size_t identifierEnd = current;
            char identifierCurrent = currentChar;

            while(isIdentifierChar(identifierCurrent) || isDigitChar(identifierCurrent)) //can contain digits too
            {
                ++identifierEnd;
                identifierCurrent = input[identifierEnd];
            }

            std::string_view name = input.substr(current, identifierEnd - current);

            //if this identifier matches a keyword, then it's not an identifier
            Keyword keyword = keywordFromString(name);

            if(keyword != Keyword::NoKeyword)
            {
                result.tokens.emplace_back(keyword);
                result.debugInfo.emplace_back(0, 0);
            }
            else
            {
                Identifier identifier
                {
                    name
                };

                result.tokens.emplace_back(identifier);
                result.debugInfo.emplace_back(0, 0);
            }

            current = identifierEnd - 1; //set it to one before end since the forloop will do +1 anyway
        }
    }

    return result;
}
