#pragma once
#include <variant>
#include <vector>
#include <string>
#include <string_view>

struct Identifier
{
    std::string_view name;
};

bool operator==(const Identifier& a, const Identifier& b);

enum class Keyword
{
    NoKeyword,
    Var,
    If,
    Else,
    For,
    Return,
    Cast
};

Keyword keywordFromString(std::string_view text);

enum class Separator
{
    CurlyBStart, CurlyBEnd, //{}
    RoundBStart, RoundBEnd, //()
    SquareBStart, SquareBEnd, //[]
    Semicolon, //;
    Comma //,
};

enum class Operator
{
    Add, //+
    Subtract, //-
    Multiply, //*
    Divide,  ///
    Assign //=
};

struct Literal
{
    std::string_view value;
};

bool operator==(const Literal& a, const Literal& b);

struct LineComment
{
    std::string_view content;
};

bool operator==(const LineComment& a, const LineComment& b);

struct BlockComment
{
    std::string_view content;
};

bool operator==(const BlockComment& a, const BlockComment& b);

using Token = std::variant<
    Identifier,
    Keyword,
    Separator,
    Operator,
    Literal,
    LineComment,
    BlockComment
>;

struct TokenDebugInfo
{
    int row;
    int column;

    TokenDebugInfo(){}
    TokenDebugInfo(int r, int c): row(r), column(c) {} 
};

struct TokenData
{
    std::vector<Token> tokens;
    std::vector<TokenDebugInfo> debugInfo;
};

std::string toString(Keyword keyword);
std::string toString(Separator separator);
std::string toString(Operator oper);
std::string toString(const Token& token);
