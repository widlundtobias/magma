#include "parser.hpp"
#include <deque>
#include <iostream>
#include <algorithm>
#include <memory>

AST parseBlock(const TokenData& tokenData)
{
    using DeclaredVariables = std::unordered_map<std::string_view, DeclaredVariable>;
    DeclaredVariables declaredVariables;
    void* arbitraryStartAddress = reinterpret_cast<void*>(0x012332434);
    size_t space = 1024 * 1024 * 1024;
    void* alignmentCursor = std::align(32, 0, arbitraryStartAddress, space);
    size_t startSpace = space;

    const std::vector<Token>& tokens = tokenData.tokens;
    AST result;
    result.nodes.reserve(tokens.size());

    //these two makes sure the range doesn't involve the scope block symbols { }
    size_t start = 1;
    size_t end = tokens.size() - 1;

    result.nodes.emplace_back(Block{0}); //statement count is unknown for now we'll set it later
    Block& thisBlock = std::get<Block>(result.nodes.back());
    size_t thisBlockIndex = result.nodes.size() - 1;

    int32_t statementCount = 0;

    struct ExpressionCreated
    {
        bool gotStatement;
        size_t endIndex;
    };

    auto newExpression = [] (size_t expressionStart, const TokenData& tokenData, DeclaredVariables& declaredVariables, AST& result)
    {
        const std::vector<Token>& tokens = tokenData.tokens;
        size_t end = tokens.size() - 1;
        size_t currentIndex = expressionStart;
        Token currentToken = tokens[currentIndex];
        bool gotStatement = false;
        while(!(currentToken == Token{Separator::Semicolon}) && static_cast<int32_t>(currentIndex) < static_cast<int32_t>(end))
        {
            gotStatement = true;
            ++currentIndex;
            currentToken = tokens[currentIndex];
        }

        parseExpression(tokenData, expressionStart, currentIndex, declaredVariables, result);

        return ExpressionCreated{gotStatement, currentIndex};
    };

    for(size_t i = start; static_cast<int32_t>(i) < static_cast<int32_t>(end); ++i)
    {
        size_t currentStatementStart = i;
        Token currentToken  = tokens[i];

        int32_t gotStatements = 0;

        if(std::holds_alternative<Keyword>(currentToken))
        {//we have a keyword to take care of
            ++gotStatements;

            Keyword keyword = std::get<Keyword>(currentToken);

            if(keyword == Keyword::Var)
            {//we have a variable declaration
                Token varType = tokens[i + 1];
                Token varName = tokens[i + 2];
                Token semicolonOrAssign = tokens[i + 3];

                if(std::holds_alternative<Identifier>(varType) && std::holds_alternative<Identifier>(varName) && (semicolonOrAssign == Token{Separator::Semicolon} || semicolonOrAssign == Token{Operator::Assign}))
                {
                    std::string_view name = std::get<Identifier>(varName).name;
                    int32_t type = 0;
                    int32_t typeSizeRequirement = 4; //get based on type id
                    int32_t typeAlignRequirement = 4; //get based on type id

                    alignmentCursor = std::align(typeAlignRequirement, typeSizeRequirement, alignmentCursor, space);
                    int32_t offset = startSpace - space;
                    space -= sizeof(int);
                    alignmentCursor = static_cast<char*>(alignmentCursor) + sizeof(int);

                    declaredVariables[name] = DeclaredVariable{type, offset};

                    result.nodes.emplace_back(VariableDeclaration{type, offset});
                    i += 2;

                    currentStatementStart = i;
                    //if semicolonOrAssign is assign, then we have initialisation of this var
                    if(semicolonOrAssign == Token{Operator::Assign})
                    {//initialisation

                        auto expressionReturn = newExpression(currentStatementStart, tokenData, declaredVariables, result);

                        i = expressionReturn.endIndex;
                        if(expressionReturn.gotStatement)
                            ++gotStatements;
                    }
                }
                else {
                    //ASEERT malformed declaration
                }
            }
            else{
                //AsSSERT unhandled keyword
            }
        }
        else
        {//if it wasn't a keyword we assume we have an expression to parse
            auto expressionReturn = newExpression(currentStatementStart, tokenData, declaredVariables, result);

            i = expressionReturn.endIndex;
            if(expressionReturn.gotStatement)
                ++gotStatements;
        }

        statementCount += gotStatements;
    }

    std::get<Block>(result.nodes[thisBlockIndex]).statementCount = statementCount;

    return result;
}

void parseExpression(const TokenData& tokens, size_t start, size_t end, const std::unordered_map<std::string_view, DeclaredVariable>& declaredVariables, AST& astOut)
{
    size_t tokenCount = end - start;
    size_t outStart = astOut.nodes.size();

    enum class ParserOperator
    {
        Add, //+
        Subtract, //-
        Multiply, //*
        Divide,  ///
        Assign,  //=
        ParenthesisStart, //(
        ParenthesisEnd, //)
    };

    //surround the whole expression with ( and ) to make the parser work without special cases for the end
    //tokens.tokens.push_back(Token{{}, 0, 0, Separator::RoundBEnd});
    std::deque<ParserOperator> operatorStack{ParserOperator::ParenthesisStart};

    auto createOperatorASTNode = [] (ParserOperator oper)
    {
        if(oper == ParserOperator::Add)
            return ASTNode{IntOperatorAdd{
            }};
        else if(oper == ParserOperator::Subtract)
            return ASTNode{IntOperatorSubtract{
            }};
        else if(oper == ParserOperator::Multiply)
            return ASTNode{IntOperatorMultiply{
            }};
        else if(oper == ParserOperator::Divide)
            return ASTNode{IntOperatorDivide{
            }};
        else if(oper == ParserOperator::Assign)
            return ASTNode{IntOperatorAssign{
            }};
    };

    auto precedence = [] (ParserOperator oper)
    {
        if(oper == ParserOperator::ParenthesisStart)
            return 0;
        else if(oper == ParserOperator::Assign)
            return 1;
        else if(oper == ParserOperator::Add)
            return 2;
        else if(oper == ParserOperator::Subtract)
            return 2;
        else if(oper == ParserOperator::Multiply)
            return 3;
        else if(oper == ParserOperator::Divide)
            return 3;
        else if(oper == ParserOperator::ParenthesisEnd)
            return 4;
    };

    auto toParserOp = [] (auto&& input)
    {
        using T = std::decay_t<decltype(input)>;
        
        if constexpr(std::is_same_v<T, Operator>)
        {
            if(input == Operator::Add)
                return ParserOperator::Add;
            else if(input == Operator::Subtract)
                return ParserOperator::Subtract;
            else if(input == Operator::Multiply)
                return ParserOperator::Multiply;
            else if(input == Operator::Divide)
                return ParserOperator::Divide;
            else if(input == Operator::Assign)
                return ParserOperator::Assign;
        }
        else
        {
            static_assert(std::is_same_v<T, Separator>);

            if(input == Separator::RoundBEnd) //end is start because we're using a reversed data set
                return ParserOperator::ParenthesisStart;
            else if(input == Separator::RoundBStart) //see above
                return ParserOperator::ParenthesisEnd;
        }
    };

    auto tokenBegin = tokens.tokens.crbegin() + tokens.tokens.size() - end;
    auto tokenEnd = tokens.tokens.crend() - start;
    for(auto tokenIter = tokenBegin; tokenIter != tokenEnd; ++tokenIter)
    {
        const Token& token = *tokenIter;

        if(std::holds_alternative<Literal>(token))
        {
            int32_t value = std::atoi(std::get<Literal>(token).value.data());

            astOut.nodes.emplace_back(ASTNode{IntConstant{value}});
        }
        else if(std::holds_alternative<Operator>(token))
        {
            ParserOperator thisOper = toParserOp(std::get<Operator>(token));
            int32_t thisPrecedence = precedence(thisOper);

            ParserOperator topOper = operatorStack.back();
            while(!operatorStack.empty() && precedence(topOper) > thisPrecedence) //associativity is swapped since we work on a reversed data set. > thisPrecedence is left associativity. >= is right
            {
                operatorStack.pop_back();

                astOut.nodes.emplace_back(createOperatorASTNode(topOper));

                topOper = operatorStack.back();
            }

            operatorStack.push_back(thisOper);
        }
        else if(std::holds_alternative<Separator>(token))
        {
            Separator separator = std::get<Separator>(token);
            if(separator == Separator::RoundBEnd)
            {
                operatorStack.emplace_back(toParserOp(separator));
            }
            else if(separator == Separator::RoundBStart)
            {
                ParserOperator topOper = operatorStack.back();

                while(topOper != ParserOperator::ParenthesisStart)
                {
                    operatorStack.pop_back();

                    astOut.nodes.emplace_back(createOperatorASTNode(topOper));

                    topOper = operatorStack.back();
                }

                operatorStack.pop_back(); //pop the ( now that it's done
            }
        }
        else if(std::holds_alternative<Identifier>(token))
        {
            Identifier identifier = std::get<Identifier>(token);
            DeclaredVariable variable = declaredVariables.at(identifier.name);

            astOut.nodes.emplace_back(ASTNode{IntVariable{variable.offset}});
        }
    }

    ParserOperator topOper = operatorStack.back();

    while(topOper != ParserOperator::ParenthesisStart)
    {
        operatorStack.pop_back();

        astOut.nodes.emplace_back(createOperatorASTNode(topOper));

        topOper = operatorStack.back();
    }
    operatorStack.pop_back(); //pop the ( now that it's done

    std::reverse(astOut.nodes.begin() + outStart, astOut.nodes.end());
}
