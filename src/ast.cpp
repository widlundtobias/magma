#include "ast.hpp"

std::string toString(const ASTNode& node)
{
    if(std::holds_alternative<IntOperatorAdd>(node))
        return "OPi+i";
    else if(std::holds_alternative<IntOperatorSubtract>(node))
        return "OPi-i";
    else if(std::holds_alternative<IntOperatorMultiply>(node))
        return "OPi*i";
    else if(std::holds_alternative<IntOperatorDivide>(node))
        return "OPi/i";
    else if(std::holds_alternative<IntOperatorAssign>(node))
        return "OPi=i";
    else if(std::holds_alternative<FloatOperatorAdd>(node))
        return "OPf+f";
    else if(std::holds_alternative<FloatOperatorSubtract>(node))
        return "OPf-f";
    else if(std::holds_alternative<FloatOperatorMultiply>(node))
        return "OPf*f";
    else if(std::holds_alternative<FloatOperatorDivide>(node))
        return "OPf/f";
    else if(std::holds_alternative<FloatOperatorAssign>(node))
        return "OPf=f";
    else if(std::holds_alternative<Block>(node))
        return "BLC[" + std::to_string(std::get<Block>(node).statementCount) + "]";
    else if(std::holds_alternative<IntNegation>(node))
        return "OP-i";
    else if(std::holds_alternative<IntToFloatCast>(node))
        return "CSTif";
    else if(std::holds_alternative<FloatNegation>(node))
        return "OP-f";
    else if(std::holds_alternative<FloatToIntCast>(node))
        return "CSTfi";
    else if(std::holds_alternative<IntConstant>(node))
        return "ic[" + std::to_string(std::get<IntConstant>(node).value) + "]";
    else if(std::holds_alternative<IntVariable>(node))
        return "iv[" + std::to_string(std::get<IntVariable>(node).offset) + "]";
    else if(std::holds_alternative<FloatConstant>(node))
        return "fc[" + std::to_string(std::get<FloatConstant>(node).value) + "]";
    else if(std::holds_alternative<FloatVariable>(node))
        return "fv[" + std::to_string(std::get<FloatVariable>(node).offset) + "]";
    else if(std::holds_alternative<VariableDeclaration>(node))
    {
        VariableDeclaration declaration = std::get<VariableDeclaration>(node);
        return "DEC[" + std::to_string(declaration.type) + ":" + std::to_string(declaration.offset) + "]";
    }
    else {//assert
    }
}
