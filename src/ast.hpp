#pragma once
#include <cstdint>
#include <variant>
#include <vector>
#include <indexof.hpp>

struct Block
{
    int32_t statementCount;
};

inline bool operator==(Block a, Block b){return a.statementCount == b.statementCount;}

struct IfElse
{
    int32_t ifBlockOffset;
    int32_t elseBlockOffset;
};

inline bool operator==(IfElse a, IfElse b){return a.ifBlockOffset == b.ifBlockOffset && a.elseBlockOffset == b.elseBlockOffset;}

struct VariableDeclaration
{
    int32_t type;
    int32_t offset;
};

inline bool operator==(VariableDeclaration a, VariableDeclaration b){return true;}

struct IntNegation
{
};

inline bool operator==(IntNegation, IntNegation){return true;}

struct FloatNegation
{
};
inline bool operator==(FloatNegation, FloatNegation){return true;}

struct IntToFloatCast
{
};
inline bool operator==(IntToFloatCast, IntToFloatCast){return true;}

struct FloatToIntCast
{
};
inline bool operator==(FloatToIntCast, FloatToIntCast){return true;}

struct IntOperatorAdd
{
};
inline bool operator==(IntOperatorAdd, IntOperatorAdd){return true;}

struct IntOperatorSubtract
{
};
inline bool operator==(IntOperatorSubtract, IntOperatorSubtract){return true;}

struct IntOperatorMultiply
{
};
inline bool operator==(IntOperatorMultiply, IntOperatorMultiply){return true;}

struct IntOperatorDivide
{
};
inline bool operator==(IntOperatorDivide, IntOperatorDivide){return true;}

struct IntOperatorAssign
{
};
inline bool operator==(IntOperatorAssign, IntOperatorAssign){return true;}

struct FloatOperatorAdd
{
};
inline bool operator==(FloatOperatorAdd, FloatOperatorAdd){return true;}

struct FloatOperatorSubtract
{
};
inline bool operator==(FloatOperatorSubtract, FloatOperatorSubtract){return true;}

struct FloatOperatorMultiply
{
};
inline bool operator==(FloatOperatorMultiply, FloatOperatorMultiply){return true;}

struct FloatOperatorDivide
{
};
inline bool operator==(FloatOperatorDivide, FloatOperatorDivide){return true;}

struct FloatOperatorAssign
{
};
inline bool operator==(FloatOperatorAssign, FloatOperatorAssign){return true;}

struct IntConstant
{
    int32_t value;
};

inline bool operator==(IntConstant a, IntConstant b)
{
    return a.value == b.value;
}

struct IntVariable
{
    int32_t offset;
};

inline bool operator==(IntVariable a, IntVariable b)
{
    return a.offset== b.offset;
}

struct FloatConstant
{
    float value;
};

inline bool operator==(FloatConstant a, FloatConstant b)
{
    return a.value == b.value;
}

struct FloatVariable
{
    float offset;
};

inline bool operator==(FloatVariable a, FloatVariable b)
{
    return a.offset== b.offset;
}

//struct FunctionCall
//{
//  std::string functionName;
//  std::vector<ASTNode> arguments;  
//};

using ASTNode = std::variant<
  IntOperatorAdd,           //0    operatorstart, binarystart
  IntOperatorSubtract,      //1
  IntOperatorMultiply,      //2
  IntOperatorDivide,        //3
  IntOperatorAssign,        //4
  FloatOperatorAdd,         //5
  FloatOperatorSubtract,    //6
  FloatOperatorMultiply,    //7
  FloatOperatorDivide,      //8
  FloatOperatorAssign,      //9
  IntNegation,              //10    unarystart, binaryend
  IntToFloatCast,           //11
  FloatNegation,            //12
  FloatToIntCast,           //13  

  Block,                    //14   block, operatorend, unaryend

  IntConstant,              //15   leavesstart, constantstart
  FloatConstant,            //16
  IntVariable,              //17   variablesstart, constantsend
  FloatVariable,            //18
  VariableDeclaration       //19   variabledeclaration, variablesend
  //--end--                 //20   leavesend, constantsend
  //FunctionCall
>;

static constexpr size_t cOperatorsStart = IndexOfV<IntOperatorAdd, ASTNode>;
static constexpr size_t cOperatorsEnd = IndexOfV<Block, ASTNode>;
static constexpr size_t cOperatorsBinaryStart = IndexOfV<IntOperatorAdd, ASTNode>;
static constexpr size_t cOperatorsBinaryEnd = IndexOfV<IntNegation, ASTNode>;
static constexpr size_t cOperatorsUnaryStart = IndexOfV<IntNegation, ASTNode>;
static constexpr size_t cOperatorsUnaryEnd = IndexOfV<Block, ASTNode>;
static constexpr size_t cConstantsStart = IndexOfV<IntConstant, ASTNode>;
static constexpr size_t cConstantsEnd = IndexOfV<VariableDeclaration, ASTNode>;
static constexpr size_t cVariablesStart = IndexOfV<IntVariable, ASTNode>;
static constexpr size_t cVariablesEnd = IndexOfV<VariableDeclaration, ASTNode>;
static constexpr size_t cLeavesStart = IndexOfV<IntConstant, ASTNode>;
static constexpr size_t cLeavesEnd = 20;

struct AST
{
    std::vector<ASTNode> nodes;
};

std::string toString(const ASTNode& node);
