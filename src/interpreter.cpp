#include "interpreter.hpp"
#include <vector>
#include <ast.hpp>

LeafFrame evaluateConstant(const ASTNode& constant, std::vector<char>& stackMemory)
{
    LeafFrame result;

    if(std::holds_alternative<IntConstant>(constant))
        result.intVal = std::get<IntConstant>(constant).value;
    else if(std::holds_alternative<FloatConstant>(constant))
        result.floatVal = std::get<FloatConstant>(constant).value;
    else if(std::holds_alternative<IntVariable>(constant))
    {
        int32_t offset = std::get<IntVariable>(constant).offset;
        int* variableMemory = reinterpret_cast<int*>(stackMemory.data() + offset); //use offset in stack

        result.intVarVal = variableMemory;
        result.intVal = *variableMemory;
    }
    else if(std::holds_alternative<FloatVariable>(constant))
    {
        int32_t offset = std::get<FloatVariable>(constant).offset;
        float* variableMemory = reinterpret_cast<float*>(stackMemory.data() + offset); //use offset in stack

        result.floatVarVal = variableMemory;
        result.floatVal = *variableMemory;
    }
    else if(std::holds_alternative<VariableDeclaration>(constant))
    {
        result.intVal = -1; //menaingless value
    }
    else {//ASSERT(false)
    }

    return result;
}

LeafFrame evaluateOperator(const ASTNode& oper, LeafFrame operand)
{
    LeafFrame result;

    if(std::holds_alternative<IntNegation>(oper))
        result.intVal = -operand.intVal;
    else if(std::holds_alternative<IntToFloatCast>(oper))
        result.floatVal = static_cast<float>(operand.intVal);
    else if(std::holds_alternative<FloatNegation>(oper))
        result.floatVal = -operand.floatVal;
    else if(std::holds_alternative<FloatToIntCast>(oper))
        result.intVal = static_cast<float>(operand.floatVal);
    else {//ASSERT(false)
    }

    return result;
}

LeafFrame evaluateOperator(const ASTNode& oper, LeafFrame left, LeafFrame right)
{
    LeafFrame result;

    //int
    if(std::holds_alternative<IntOperatorAdd>(oper))
        result.intVal = left.intVal + right.intVal;
    else if(std::holds_alternative<IntOperatorSubtract>(oper))
        result.intVal = left.intVal - right.intVal;
    else if(std::holds_alternative<IntOperatorMultiply>(oper))
        result.intVal = left.intVal * right.intVal;
    else if(std::holds_alternative<IntOperatorDivide>(oper))
        result.intVal = left.intVal / right.intVal;
    else if(std::holds_alternative<IntOperatorAssign>(oper))
    {
        *left.intVarVal = right.intVal;
        result.intVal = right.intVal;
    }
    //float
    else if(std::holds_alternative<FloatOperatorAdd>(oper))
        result.floatVal = left.floatVal + right.floatVal;
    else if(std::holds_alternative<FloatOperatorSubtract>(oper))
        result.floatVal = left.floatVal - right.floatVal;
    else if(std::holds_alternative<FloatOperatorMultiply>(oper))
        result.floatVal = left.floatVal * right.floatVal;
    else if(std::holds_alternative<FloatOperatorDivide>(oper))
        result.floatVal = left.floatVal / right.floatVal;
    else if(std::holds_alternative<FloatOperatorAssign>(oper))
    {
        *left.floatVarVal = right.floatVal;
        result.floatVal = right.floatVal;
    }
    else {//ASSERT(false)
    }

    return result;
}

std::optional<LeafFrame> interpret(const AST& ast)
{
    std::vector<char> stackMemory(1024 * 1024);
    std::vector<ParentFrame> parentStack;
    std::vector<LeafFrame> leafStack;

    parentStack.reserve(2048);
    leafStack.reserve(2048);

    size_t size = ast.nodes.size();
    for(size_t i = 0; i < size; ++i)
    {
        const ASTNode& node = ast.nodes[i];
        size_t typeIndex = node.index();

        if(bool isConstant = typeIndex >= cLeavesStart && typeIndex < cLeavesEnd; isConstant)
        {//if node is leaf push on leaf stack, then resolve as many parents as we can
            leafStack.push_back(evaluateConstant(node, stackMemory));

            if(parentStack.empty())
                continue;

            ParentFrame nextParent = parentStack.back();
            int32_t nextLeafCount = leafStack.size() - nextParent.leafIndex;
            bool canResolveNext = nextParent.arity - nextLeafCount == 0;

            while(canResolveNext)
            {
                int32_t arity = nextParent.arity;
                const ASTNode& parentToResolve = ast.nodes[nextParent.parentIndex];
                size_t parentToResolveTypeIndex = parentToResolve.index();

                if(std::holds_alternative<Block>(parentToResolve))
                {//if the parent is a block node, then we simply discard all its children
                    std::cout << "block with " << arity << " statements evaluated. Results in reverse order:\n";
                    for(int32_t i = 0; i < arity; ++i)
                    {
                        std::cout << "statement " << arity - i - 1<< " was: " << leafStack.back().intVal << "\n";
                        leafStack.pop_back();
                    }
                    std::cout << "\n";
                }
                else if(std::holds_alternative<VariableDeclaration>(parentToResolve))
                {//if the parent is a declaration node, then we do nothing, it has no children
                    std::cout << "decl\n";
                }
                else if(/*function*/false)
                {//if the parent is a function node, extract params and run
                    //TBI
                }
                else if(parentToResolveTypeIndex >= cOperatorsStart && parentToResolveTypeIndex < cOperatorsEnd)
                {//if the parent is an operator, we either give it 1 or 2 operands depending on arity, the run
                    size_t leafStackSize = leafStack.size();
                    if(arity == 1)
                    {//unary operator grabs 1 operand and runs
                        LeafFrame arg1 = leafStack[leafStack.size() - 1];
                        LeafFrame newLeaf = evaluateOperator(parentToResolve, arg1);

                        leafStack.pop_back();
                        leafStack.push_back(newLeaf);
                    }
                    else
                    {//binary operator grabs 2 operands and runs
                        LeafFrame arg1 = leafStack[leafStackSize - 2];
                        LeafFrame arg2 = leafStack[leafStackSize - 1];
                        LeafFrame newLeaf = evaluateOperator(parentToResolve, arg1, arg2);

                        leafStack.pop_back();
                        leafStack.pop_back();
                        leafStack.push_back(newLeaf);
                    }
                }
                else {
                    //ASSERT
                }

                parentStack.pop_back();

                if(parentStack.empty())
                    break;

                //prepare for next potential parent
                nextParent = parentStack.back();
                nextLeafCount = leafStack.size() - nextParent.leafIndex;
                canResolveNext = nextParent.arity - nextLeafCount == 0;
            }
        }
        else
        {//if node is parent we need to push it on the parentStack, keeping some information
            int16_t arity = -1;
            
            if(std::holds_alternative<Block>(node))
            {//if the parent is a block node, statement count is arity
                arity = std::get<Block>(node).statementCount;
            }
            else if(std::holds_alternative<VariableDeclaration>(node))
            {//if the parent is a decl node, the arity is 0
                //do allocation logic here
                arity = 0;
            }
            else if(/*function*/false)
            {//if the parent is a function node, param count is arity
                //TBI
            }
            else if(typeIndex >= cOperatorsStart && typeIndex < cOperatorsEnd)
            {//operators have 1 or 2 arity
                arity = typeIndex >= cOperatorsBinaryStart && typeIndex < cOperatorsBinaryEnd ? 2 : 1;
            }

            //ASSERT ARITY != -1

            ParentFrame frame
            {
                static_cast<uint32_t>(i),
                static_cast<uint16_t>(leafStack.size()),
                static_cast<int16_t>(arity),
            };

            parentStack.emplace_back(frame);
        }
    }

    //ASSERT 0 or 1 size
    return leafStack.empty() ? std::optional<LeafFrame>{} : std::optional<LeafFrame>{leafStack.back()};
}
