#include <iostream>
#include <variant>
#include <type_traits>

template <std::size_t, typename T, typename... Ts>
struct IndexOfHelper;

template <std::size_t I, typename T, typename... Rest>
struct IndexOfHelper<I, T, T, Rest...>
    : std::integral_constant<std::size_t, I> { };
    
template <std::size_t I, typename T, typename First, typename... Rest>
struct IndexOfHelper<I, T, First, Rest...> 
    : IndexOfHelper<I + 1, T, Rest...> { };
    
template <typename T, typename V>
struct IndexOf;

template <typename T, typename... Ts>
struct IndexOf<T, std::variant<Ts...>> : IndexOfHelper<0, T, Ts...> { };

template <typename T, typename V>
constexpr std::size_t IndexOfV = IndexOf<T, V>::value;
