#pragma once
#include <cstdint>
#include <cstddef>
#include <optional>

struct ParentFrame
{
    uint32_t parentIndex;
    uint16_t leafIndex;
    int16_t arity;
};

struct LeafFrame
{
    union
    {
        int32_t intVal;
        float floatVal;
    };
    union
    {
        int32_t* intVarVal;
        float* floatVarVal;
    };
};

std::optional<LeafFrame> interpret(const struct AST& ast);
