#pragma once
#include <cstdint>
#include <vector>
#include <string_view>
#include <unordered_map>
#include <token.hpp>
#include <ast.hpp>

struct DeclaredVariable
{
    int32_t type;
    int32_t offset;
};

AST parseBlock(const TokenData& tokens); //produces a prefix-style tree with a block node as the root and all statements as children. internal expressions are pre-fix too
void parseExpression(const TokenData& tokens, size_t start, size_t end, const std::unordered_map<std::string_view, DeclaredVariable>& declaredVariables, AST& astOut); //produces a pre-fix tree (AKA polish notation) for the expression
