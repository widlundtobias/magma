#include <catch.hpp>
#include <tokenizer.hpp>
#include <parser.hpp>

TEST_CASE("parser can parse arithmetic expressions", "[parser]")
{
    std::string expressionString = "1 + 3 * (4 - 2) + 4 * (1 + (2 * ((3 + 2))))";

    TokenData tokens = tokenize(expressionString);

    AST tree;
    parseExpression(tokens, 0, tokens.tokens.size(), {}, tree);

    REQUIRE(tree.nodes.size() == 17);

    //required prefix tree:
    //+ + 1 * 3 - 4 2 * 4 + 1 * 2 + 3 2
    //+ + 1 * 3 2 * 4 + 1 * 2 + 3 2
    //+ + 1 6 * 4 + 1 * 2 + 3 2
    //+ 7 * 4 + 1 * 2 + 3 2
    //+ 7 * 4 + 1 * 2 5
    //+ 7 * 4 + 1 10
    //+ 7 * 4 11
    //+ 7 44
    //51

    ASTNode lit1 = ASTNode{IntConstant{1}};
    ASTNode lit2 = ASTNode{IntConstant{2}};
    ASTNode lit3 = ASTNode{IntConstant{3}};
    ASTNode lit4 = ASTNode{IntConstant{4}};

    ASTNode add = ASTNode{IntOperatorAdd{}};
    ASTNode sub = ASTNode{IntOperatorSubtract{}};
    ASTNode mul = ASTNode{IntOperatorMultiply{}};

    REQUIRE(tree.nodes[0] == add);
    REQUIRE(tree.nodes[1] == add);
    REQUIRE(tree.nodes[2] == lit1);
    REQUIRE(tree.nodes[3] == mul);
    REQUIRE(tree.nodes[4] == lit3);
    REQUIRE(tree.nodes[5] == sub);
    REQUIRE(tree.nodes[6] == lit4);
    REQUIRE(tree.nodes[7] == lit2);
    REQUIRE(tree.nodes[8] == mul);
    REQUIRE(tree.nodes[9] == lit4);
    REQUIRE(tree.nodes[10] == add);
    REQUIRE(tree.nodes[11] == lit1);
    REQUIRE(tree.nodes[12] == mul);
    REQUIRE(tree.nodes[13] == lit2);
    REQUIRE(tree.nodes[14] == add);
    REQUIRE(tree.nodes[15] == lit3);
    REQUIRE(tree.nodes[16] == lit2);
}

TEST_CASE("single numbers are valid expressions", "[parser]")
{
    std::string expressionString = "3";

    TokenData tokens = tokenize(expressionString);

    AST tree;
    parseExpression(tokens, 0, tokens.tokens.size(), {}, tree);

    REQUIRE(tree.nodes.size() == 1);

    //required prefix tree:
    //3

    ASTNode lit3 = ASTNode{IntConstant{3}};

    REQUIRE(tree.nodes[0] == lit3);
}

TEST_CASE("parser can parse blocks of statements", "[parser]")
{
    std::string blockString = "{1 + 3; (4 - 2) + 4; (1 + (2 * ((3 + 2))));}";

    TokenData tokens = tokenize(blockString);

    AST tree = parseBlock(tokens);

    //required AST tree:
    //BLC[3] OPi+i i[1] i[3] OPi+i OPi-i i[4] i[2] i[4] OPi+i i[1] OPi*i i[2] OPi+i i[3] i[2]
    //BLC[3] OPi+i i[1] i[3] OPi+i i[2] i[4] OPi+i i[1] OPi*i i[2] OPi+i i[3] i[2]
    //BLC[3] i[4] OPi+i i[2] i[4] OPi+i i[1] OPi*i i[2] OPi+i i[3] i[2]
    //BLC[3] i[4] OPi+i i[2] i[4] OPi+i i[1] OPi*i i[2] i[5]
    //BLC[3] i[4] [6] OPi+i i[1] OPi*i i[2] i[5]
    //BLC[3] i[4] [6] OPi+i i[1] i[10]
    //BLC[3] i[4] [6] i[11]


    REQUIRE(tree.nodes.size() == 16);

    ASTNode lit1 = ASTNode{IntConstant{1}};
    ASTNode lit2 = ASTNode{IntConstant{2}};
    ASTNode lit3 = ASTNode{IntConstant{3}};
    ASTNode lit4 = ASTNode{IntConstant{4}};

    ASTNode add = ASTNode{IntOperatorAdd{}};
    ASTNode sub = ASTNode{IntOperatorSubtract{}};
    ASTNode mul = ASTNode{IntOperatorMultiply{}};

    ASTNode block3 = ASTNode{Block{3}};

    REQUIRE(tree.nodes[0] == block3);
    REQUIRE(tree.nodes[1] == add);
    REQUIRE(tree.nodes[2] == lit1);
    REQUIRE(tree.nodes[3] == lit3);
    REQUIRE(tree.nodes[4] == add);
    REQUIRE(tree.nodes[5] == sub);
    REQUIRE(tree.nodes[6] == lit4);
    REQUIRE(tree.nodes[7] == lit2);
    REQUIRE(tree.nodes[8] == lit4);
    REQUIRE(tree.nodes[9] == add);
    REQUIRE(tree.nodes[10] == lit1);
    REQUIRE(tree.nodes[11] == mul);
    REQUIRE(tree.nodes[12] == lit2);
    REQUIRE(tree.nodes[13] == add);
    REQUIRE(tree.nodes[14] == lit3);
    REQUIRE(tree.nodes[15] == lit2);
}

TEST_CASE("parser can handle crazy but valid semicolon setups", "[parser]")
{
    std::string blockStringSane = "{1 + 3; (4 - 2) + 4; (1 + (2 * ((3 + 2))));}";
    std::string blockStringCrazy1 = "{;;1 + 3; (4 - 2) + 4; (1 + (2 * ((3 + 2))));}";
    std::string blockStringCrazy2 = "{;1 + 3; (4 - 2) + 4;;;; (1 + (2 * ((3 + 2))));;;;;}";

    TokenData tokensSane = tokenize(blockStringSane);
    TokenData tokensCrazy1 = tokenize(blockStringCrazy1);
    TokenData tokensCrazy2 = tokenize(blockStringCrazy2);

    REQUIRE(parseBlock(tokensSane).nodes == parseBlock(tokensCrazy1).nodes);
    REQUIRE(parseBlock(tokensSane).nodes == parseBlock(tokensCrazy2).nodes);
}

TEST_CASE("empty blocks are OK", "[parser]")
{
    std::string input = "{}";

    TokenData tokens = tokenize(input);
    AST tree = parseBlock(tokens);

    ASTNode block0 = ASTNode{Block{0}};

    REQUIRE(tree.nodes.size() == 1);
    REQUIRE(tree.nodes[0] == block0);
}
