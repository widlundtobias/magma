#include <catch.hpp>
#include <ast.hpp>
#include <interpreter.hpp>
#include <tokenizer.hpp>
#include <parser.hpp>

TEST_CASE("sequential subtractions work", "[expression]")
{
    std::string expressionString = "1 - 1 - 10";

    TokenData tokenizedExpression = tokenize(expressionString);

    AST expressionAST;
    parseExpression(tokenizedExpression, 0, tokenizedExpression.tokens.size(), {}, expressionAST);

    std::optional<LeafFrame> result = interpret(expressionAST);

    REQUIRE(result);
    REQUIRE(result->intVal == -10);
}
