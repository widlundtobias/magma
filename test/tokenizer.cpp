#include <catch.hpp>
#include <tokenizer.hpp>

TEST_CASE("tokenizer can tokenize arithmetic expressions", "[tokenizer]")
{
    std::string input = "1 + xyz0 * (50.0f/100.0)-3000";

    TokenData data = tokenize(input);
    const std::vector<Token>& tokens = data.tokens;

    REQUIRE(tokens.size() == 11);

    Token current = tokens[0];
    REQUIRE(std::holds_alternative<Literal>(current));
    REQUIRE(std::get<Literal>(current).value == "1");

    current = tokens[1];
    REQUIRE(std::holds_alternative<Operator>(current));
    REQUIRE(std::get<Operator>(current) == Operator::Add);

    current = tokens[2];
    REQUIRE(std::holds_alternative<Identifier>(current));
    REQUIRE(std::get<Identifier>(current).name == "xyz0");

    current = tokens[3];
    REQUIRE(std::holds_alternative<Operator>(current));
    REQUIRE(std::get<Operator>(current) == Operator::Multiply);

    current = tokens[4];
    REQUIRE(std::holds_alternative<Separator>(current));
    REQUIRE(std::get<Separator>(current) == Separator::RoundBStart);

    current = tokens[5];
    REQUIRE(std::holds_alternative<Literal>(current));
    REQUIRE(std::get<Literal>(current).value == "50.0f");

    current = tokens[6];
    REQUIRE(std::holds_alternative<Operator>(current));
    REQUIRE(std::get<Operator>(current) == Operator::Divide);

    current = tokens[7];
    REQUIRE(std::holds_alternative<Literal>(current));
    REQUIRE(std::get<Literal>(current).value == "100.0");

    current = tokens[8];
    REQUIRE(std::holds_alternative<Separator>(current));
    REQUIRE(std::get<Separator>(current) == Separator::RoundBEnd);

    current = tokens[9];
    REQUIRE(std::holds_alternative<Operator>(current));
    REQUIRE(std::get<Operator>(current) == Operator::Subtract);

    current = tokens[10];
    REQUIRE(std::holds_alternative<Literal>(current));
    REQUIRE(std::get<Literal>(current).value == "3000");
}

TEST_CASE("tokenizer can reverse tokenized symbols in input", "[tokenizer]")
{
    std::string input = "1 + xyz0 * (50.0f/100.0)-3000";

    TokenData data = tokenize(input);
    std::vector<Token>& tokens = data.tokens;

    std::reverse(tokens.begin(), tokens.end());

    Token current = tokens[0];
    REQUIRE(std::holds_alternative<Literal>(current));
    REQUIRE(std::get<Literal>(current).value == "3000");

    current = tokens[1];
    REQUIRE(std::holds_alternative<Operator>(current));
    REQUIRE(std::get<Operator>(current) == Operator::Subtract);

    current = tokens[2];
    REQUIRE(std::holds_alternative<Separator>(current));
    REQUIRE(std::get<Separator>(current) == Separator::RoundBEnd);

    current = tokens[3];
    REQUIRE(std::holds_alternative<Literal>(current));
    REQUIRE(std::get<Literal>(current).value == "100.0");

    current = tokens[4];
    REQUIRE(std::holds_alternative<Operator>(current));
    REQUIRE(std::get<Operator>(current) == Operator::Divide);

    current = tokens[5];
    REQUIRE(std::holds_alternative<Literal>(current));
    REQUIRE(std::get<Literal>(current).value == "50.0f");

    current = tokens[6];
    REQUIRE(std::holds_alternative<Separator>(current));
    REQUIRE(std::get<Separator>(current) == Separator::RoundBStart);

    current = tokens[7];
    REQUIRE(std::holds_alternative<Operator>(current));
    REQUIRE(std::get<Operator>(current) == Operator::Multiply);

    current = tokens[8];
    REQUIRE(std::holds_alternative<Identifier>(current));
    REQUIRE(std::get<Identifier>(current).name == "xyz0");

    current = tokens[9];
    REQUIRE(std::holds_alternative<Operator>(current));
    REQUIRE(std::get<Operator>(current) == Operator::Add);

    current = tokens[10];
    REQUIRE(std::holds_alternative<Literal>(current));
    REQUIRE(std::get<Literal>(current).value == "1");
}
