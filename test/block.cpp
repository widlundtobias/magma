#include <catch.hpp>
#include <ast.hpp>
#include <interpreter.hpp>
#include <tokenizer.hpp>
#include <parser.hpp>

TEST_CASE("parsing and interpreting blocks of code works", "[block]")
{
    std::string blockString = "{1 - 1; 10 * 18 + 4; 67/3*3-2;}";

    TokenData tokenizedBlock = tokenize(blockString);

    AST blockAST = parseBlock(tokenizedBlock);

    std::optional<LeafFrame> result = interpret(blockAST);

    REQUIRE(!result);
}

TEST_CASE("parsing and interpreting empty/single-value blocks work", "[block]")
{
    std::optional<LeafFrame> result = interpret(parseBlock(tokenize("{}")));
    REQUIRE(!result);
    result = interpret(parseBlock(tokenize("{4}")));
    REQUIRE(!result);
}
